from airflow import DAG
from datetime import datetime, timedelta
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.contrib.kubernetes import secret
from airflow import configuration as conf

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2020, 4, 22),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'default_view': 'graph',
}

namespace = conf.get('kubernetes', 'NAMESPACE')
in_cluster=True
config_file=None

dag = DAG('task_2',
          schedule_interval='@once',
          default_args=default_args)

def get_credentials(secret_name, keys):
    """Returns list with definited Kubernetes Secrets mapped to same env variables
    Input:
    1. secret_name - string with name of Kubernetes Secret
    2. keys - list of keys to take from secret
    Output:
    1. result - list with defined secrets
    """
    result = []
    for key in keys:
        secret_env = secret.Secret(
        deploy_type='env',
        deploy_target=key,
        secret=secret_name,
        key=key)
        result.append(secret_env)
    return result

with dag:
    artists_upload_operator = KubernetesPodOperator(
        namespace=namespace,
        image = 'registry.gitlab.com/overbryd/docker-embulk:latest',
        image_pull_policy = 'IfNotPresent',
        arguments=["run", "tasks/example-csv.yml.liquid"],
        name="airflow-task2",
        task_id = 'upload_files',
        secrets = get_credentials('credentials', ['PGHOST', 'PGUSER', 'PGPASSWORD']),
        env_vars = {
            'PGDATABASE': 'target',
            'PGSCHEMA': 'public',
            'PGTABLE': 'artists',
        },
        in_cluster=in_cluster,
        cluster_context='docker-for-desktop',
        config_file=config_file,
        is_delete_operator_pod=True,
        get_logs=True)
